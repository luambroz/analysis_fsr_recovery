from __future__ import division
import ROOT
from ROOT import *
from array import array
import math
import os
import sys

plot_names = ["mBBJ", "pTJ3", "MV2c10J3", "j3Flav", "HT", "SumPtJet", "MET", "SumMV2c10", "dR_bj_min", "dR_bj_max", "dR_j_bb", "dR_bj_max_Div_dR_bb", "dEta_bj_max_Div_dEta_bb", "dPhi_bj_max_Div_dPhi_bb", "pt3_Div_ptb_min", "pt3_Div_ptb_max", "pt3_Div_ptbb", "pt3_Div_ptb_closest_b", "pt3_Div_ptb_farthest_b", "z_star", "dR_bj_min_dR_bj_max", "pt3_Div_ptb_closest_b_pt3_Div_ptb_farthest_b", "pt3_Div_ptb_max_pt3_Div_ptb_min", "Eta_bj_max_Div_dEta_bb_dPhi_bj_max_Div_dPhi_bb", "abs_etaJ3_etaB1_acos_cos_phiJ3_phiB1", "mBB_mBBJ"]

gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(True)
gStyle.SetPaintTextFormat("4.1f")

###############
showBkg = False
###############

def main():

  file_output_qqZvvHbb = ROOT.TFile("qqZvvHbb_output.root")
  file_output_ttbar    = ROOT.TFile("ttbar_output.root")
  file_output_Znunu    = ROOT.TFile("Znunu_output.root")

  ### S/B FSR-PU ###
  for plot_name in plot_names:
    h_FSR = file_output_qqZvvHbb.Get("h_qqZvvHbb_" + plot_name + "_FSR")
    h_PU  = file_output_qqZvvHbb.Get("h_qqZvvHbb_" + plot_name + "_PU")
    h_low = file_output_qqZvvHbb.Get("h_qqZvvHbb_" + plot_name + "_low")
    h_HS  = file_output_qqZvvHbb.Get("h_qqZvvHbb_" + plot_name + "_HS")
    
    h_all_qqZvvHbb = file_output_qqZvvHbb.Get("h_qqZvvHbb_" + plot_name + "_all")
    h_all_ttbar    = file_output_ttbar.Get("h_ttbar_" + plot_name + "_all")
    h_all_Znunu    = file_output_Znunu.Get("h_Znunu_" + plot_name + "_all")

    ###
    scale = 100
    h_all_ttbar.Scale(1./scale)
    h_all_Znunu.Scale(1./scale)
    ###

    if h_all_qqZvvHbb.ClassName() == "TH1F":
      c = TCanvas("c_" + plot_name, "c_" + plot_name, 1000, 1000)

      h_all_qqZvvHbb.GetYaxis().SetTitle("Yields")

      h_FSR.SetLineWidth(2)
      h_PU.SetLineWidth(2) 
      h_low.SetLineWidth(2)
      h_HS.SetLineWidth(2) 
      h_all_qqZvvHbb.SetLineWidth(2)
      h_all_ttbar.SetLineWidth(2)
      h_all_Znunu.SetLineWidth(2)

      h_FSR.SetLineColor(kRed)
      h_PU.SetLineColor(kBlue)
      h_low.SetLineColor(kGreen)
      h_HS.SetLineColor(6)
      h_all_qqZvvHbb.SetLineColor(kBlack)
      h_all_ttbar.SetLineColor(kAzure+8)
      h_all_Znunu.SetLineColor(kOrange-3)

      h_all_qqZvvHbb.Draw("HIST")
      h_FSR.Draw("HIST SAME")
      h_PU.Draw("HIST SAME") 
      h_low.Draw("HIST SAME")
      h_HS.Draw("HIST SAME")  
      if showBkg:
        h_all_ttbar.Draw("HIST SAME")  
        h_all_Znunu.Draw("HIST SAME")  

      leg = TLegend(0.40, 0.6, 0.90, 0.90)
      #leg = TLegend(0.60, 0.7, 0.90, 0.90)
      leg.SetShadowColor(10)
      leg.SetFillStyle(0)
      leg.SetBorderSize(0)
      leg.AddEntry(h_all_qqZvvHbb, "all yield: " + str(round(h_all_qqZvvHbb.Integral(0,-1),2)) + ", mean: " + str(round(h_all_qqZvvHbb.GetMean(),1)), "lp")
      leg.AddEntry(h_FSR, "FSR yield: " + str(round(h_FSR.Integral(0,-1),2)) + ", mean: " + str(round(h_FSR.GetMean(),1)), "lp")
      leg.AddEntry(h_PU, "PU yield: " + str(round(h_PU.Integral(0,-1),2)) + ", mean: " +  str(round(h_PU.GetMean(),1)), "lp")
      leg.AddEntry(h_low, "low yield: " + str(round(h_low.Integral(0,-1),2)) + ", mean: " + str(round(h_low.GetMean(),1)), "lp")
      leg.AddEntry(h_HS, "HS yield: " + str(round(h_HS.Integral(0,-1),2)) + ", mean: " + str(round(h_HS.GetMean(),1)), "lp")
      if showBkg:
        leg.AddEntry(h_all_ttbar, "ttbar yield: " + str(round(h_all_ttbar.Integral(0,-1),2) * scale) + ", mean: " + str(round(h_all_ttbar.GetMean(),1)) + ". Shape scaled down by: " + str(scale), "lp")
        leg.AddEntry(h_all_Znunu, "Znunu yield: " + str(round(h_all_Znunu.Integral(0,-1),2) * scale) + ", mean: " + str(round(h_all_Znunu.GetMean(),1)) + ". Shape scaled down by: " + str(scale), "lp")
      leg.Draw("SAME")

      c.SaveAs("./OneDplots/" + plot_name + ".pdf")
      c.SaveAs("./OneDplots/" + plot_name + ".png")

      del c, leg
    
    else:
      c1 = TCanvas("c1_" + plot_name, "c1_" + plot_name, 1000, 1000)
      c1.cd()
      h_FSR.SetTitle("FSR yields")
      h_FSR.Draw("COLZ0 TEXT")
      c1.SaveAs("./TwoDplots/" + plot_name + "_S.pdf")
      c1.SaveAs("./TwoDplots/" + plot_name + "_S.png")
      del c1

      c2 = TCanvas("c2_" + plot_name, "c2_" + plot_name, 1000, 1000)
      c2.cd()
      h_PU2 = h_PU.Clone("h_PU2")
      h_FSR2 = h_FSR.Clone("h_FSR2")
      h_HS2 = h_HS.Clone("h_HS2")
      h_PU2.Add(h_HS2)
      h_FSR2.Divide(h_PU2)
      h_FSR2.SetTitle("FSR / (PU + HS)")
      h_FSR2.Draw("COLZ0 TEXT")
      c2.SaveAs("./TwoDplots/" + plot_name + "_SoB.pdf")
      c2.SaveAs("./TwoDplots/" + plot_name + "_SoB.png")
      del c2

      c4 = TCanvas("c4_" + plot_name, "c4_" + plot_name, 1000, 1000)
      c4.cd()
      h_PU4 = h_PU.Clone("h_PU4")
      h_FSR4 = h_FSR.Clone("h_FSR4")
      h_HS4 = h_HS.Clone("h_HS4")
      h_low4 = h_low.Clone("h_low4")
      h_FSR4.Add(h_low4)
      h_PU4.Add(h_HS4)
      h_FSR4.Divide(h_PU4)
      h_FSR4.SetTitle("(FSR + low) / (PU + HS)")
      h_FSR4.Draw("COLZ0 TEXT")
      c4.SaveAs("./TwoDplots/" + plot_name + "_with_low_SoB.pdf")
      c4.SaveAs("./TwoDplots/" + plot_name + "_with_low_SoB.png")
      del c4

      c3 = TCanvas("c3_" + plot_name, "c3_" + plot_name, 1000, 1000)
      c3.cd()
      h_PU3 = h_PU.Clone("h_PU3")
      h_FSR3 = h_FSR.Clone("h_FSR3")
      h_HS3 = h_HS.Clone("h_HS3")

      h_cumulative3 = h_FSR3.Clone("cumulative3")
      h_cumulative3.Reset()
      for i in range(1, h_cumulative3.GetNbinsX() + 1):
        for j in range(1, h_cumulative3.GetNbinsY() + 1):
          den3 = h_HS3.Integral(1, i, 1, j) + h_PU3.Integral(1, i, 1, j)
          if den3 > 0:
            h_cumulative3.SetBinContent(i, j, h_FSR3.Integral(1,i,1,j) / den3 )
          else:
            h_cumulative3.SetBinContent(i, j, 0)

      h_cumulative3.SetTitle("FSR / (PU + HS) cumulative")
      h_cumulative3.Draw("COLZ0 TEXT")
      c3.SaveAs("./TwoDplots/" + plot_name + "_SoB_cumulative.pdf")
      c3.SaveAs("./TwoDplots/" + plot_name + "_SoB_cumulative.png")
      del c3


    del h_all_qqZvvHbb, h_FSR, h_PU, h_HS, h_low



if __name__ == "__main__":
  main()
