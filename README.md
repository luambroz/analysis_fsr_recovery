# Description
Scripts to study the Final State Radiation in the VH(bb) analysis.
![](FSR_TwoLep.gif)
# Instructions  
To compile the code:
```
make
```
and then to run it:
```
./execute
```
This will take care of looping over the ntuples (in the VHbb style) and running the plotting macro. The input file is assumed to be called `qqZvvHbbJ_PwPy8MINLO-FSR.root` and to be in this directory. Alternatively, `MVA.ipynb` can be used to import the ntuples into a Panda DataFrame.
