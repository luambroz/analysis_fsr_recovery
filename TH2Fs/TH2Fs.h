#include "TH2F.h"
#include "TFile.h"
#include "TList.h"
#include <iostream>
#include <vector>
#include <map>

using namespace std;

class TH2Fs {
  
    TString m_name;
    TH2F *m_h_PU;
    TH2F *m_h_HS;
    TH2F *m_h_low;
    TH2F *m_h_FSR;
    TH2F *m_h_all;

 public:
    
    TH2Fs(){
      m_name="null";
    };
    
    TH2Fs(TString histname, TString title, int xnbins, float xmin, float xmax, int ynbins, float ymin, float ymax, TString xaxis_title = "", TString yaxis_title = ""){
      m_name = histname;
      m_h_PU  = new TH2F(histname + "_PU",  title,  xnbins, xmin, xmax, ynbins, ymin, ymax);
      m_h_HS  = new TH2F(histname + "_HS",  title,  xnbins, xmin, xmax, ynbins, ymin, ymax);
      m_h_low = new TH2F(histname + "_low", title, xnbins, xmin, xmax, ynbins, ymin, ymax);
      m_h_FSR = new TH2F(histname + "_FSR", title, xnbins, xmin, xmax, ynbins, ymin, ymax);
      m_h_all = new TH2F(histname + "_all", title, xnbins, xmin, xmax, ynbins, ymin, ymax);
      if(xaxis_title != ""){
        m_h_PU->GetXaxis()->SetTitle(xaxis_title);
        m_h_HS->GetXaxis()->SetTitle(xaxis_title);
        m_h_low->GetXaxis()->SetTitle(xaxis_title);
        m_h_FSR->GetXaxis()->SetTitle(xaxis_title);
        m_h_all->GetXaxis()->SetTitle(xaxis_title);
      }
      if(yaxis_title != ""){
        m_h_PU->GetYaxis()->SetTitle(yaxis_title);
        m_h_HS->GetYaxis()->SetTitle(yaxis_title);
        m_h_low->GetYaxis()->SetTitle(yaxis_title);
        m_h_FSR->GetYaxis()->SetTitle(yaxis_title);
        m_h_all->GetYaxis()->SetTitle(yaxis_title);
      }
    }
    
  void Fill(double valx, double valy, double weight, int FSRFlag); 
  void Write();

};

