#include "TH1F.h"
#include "TFile.h"
#include "TList.h"
#include <iostream>
#include <vector>
#include <map>

using namespace std;

class TH1Fs {
  
    TString m_name;
    TH1F *m_h_PU;
    TH1F *m_h_HS;
    TH1F *m_h_low;
    TH1F *m_h_FSR;
    TH1F *m_h_all;

 public:
    
    TH1Fs(){
      m_name="null";
    };
    
    TH1Fs(TString histname, TString title,int nbins, float xmin, float xmax, TString xaxis_title = ""){
      m_name = histname;
      m_h_PU  = new TH1F(histname + "_PU",  title,  nbins, xmin, xmax);
      m_h_HS  = new TH1F(histname + "_HS",  title,  nbins, xmin, xmax);
      m_h_low = new TH1F(histname + "_low", title, nbins, xmin, xmax);
      m_h_FSR = new TH1F(histname + "_FSR", title, nbins, xmin, xmax);
      m_h_all = new TH1F(histname + "_all", title, nbins, xmin, xmax);
      if(xaxis_title != ""){
        m_h_PU->GetXaxis()->SetTitle(xaxis_title);
        m_h_HS->GetXaxis()->SetTitle(xaxis_title);
        m_h_low->GetXaxis()->SetTitle(xaxis_title);
        m_h_FSR->GetXaxis()->SetTitle(xaxis_title);
        m_h_all->GetXaxis()->SetTitle(xaxis_title);
      }
    }
    
  void Fill(double val, double weight, int FSRFlag); 
  void Write();

};

