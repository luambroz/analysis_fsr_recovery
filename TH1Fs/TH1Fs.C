#include "TH1Fs.h"


void TH1Fs::Fill(double val, double weight, int FSRFlag){

  if(FSRFlag == 0) m_h_PU->Fill(val, weight);
  if(FSRFlag == 2) m_h_HS->Fill(val, weight);
  if(FSRFlag == 1) m_h_low->Fill(val, weight);
  if(FSRFlag == 3) m_h_FSR->Fill(val, weight);
  m_h_all->Fill(val, weight);

}

void TH1Fs::Write(){

  m_h_PU->Write();
  m_h_HS->Write();
  m_h_low->Write();
  m_h_FSR->Write();
  m_h_all->Write();  

};
