from __future__ import division
import ROOT
from ROOT import *
from array import array
import math
import os
import sys

gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(True)
gStyle.SetPaintTextFormat("4.1f")

samples = ["qqZvvHbb", "ttbar", "Znunu"]
jets = ["2","3"]

for jet in jets:
  for sample in samples:

    c = TCanvas("2","2",800,900)
    pad_1=TPad("pad_1", "up", 0., 0.25, 1., 1.)
    pad_1.SetBottomMargin(0.02)
    pad_1.Draw()
    pad_2=TPad("pad_2", "down", 0.0, 0.0, 1.0, 0.25)
    pad_2.SetTopMargin(0.02)
    pad_2.SetBottomMargin(0.3)
    pad_2.Draw();
    pad_1.cd()

    file_output = ROOT.TFile(sample + "_output.root")

    h_mBB_2tag2jet         = file_output.Get("h_" + sample + "_mBB_2tag" + jet + "jet")
    h_mBB_2tag2jet_withFSR = file_output.Get("h_" + sample + "_mBB_2tag" + jet + "jet_withFSR")

    rebin = 2
    h_mBB_2tag2jet.Rebin(rebin)
    h_mBB_2tag2jet_withFSR.Rebin(rebin)

    h_mBB_2tag2jet.SetLineWidth(2)
    h_mBB_2tag2jet_withFSR.SetLineWidth(2)

    h_mBB_2tag2jet_withFSR.SetLineColor(kRed)
    h_mBB_2tag2jet_withFSR.GetXaxis().SetTitle("mBB [GeV]")
    h_mBB_2tag2jet_withFSR.GetYaxis().SetTitle("Yields")
    h_mBB_2tag2jet_withFSR.SetTitle(sample + "2tag"+ jet +"jet")
    h_mBB_2tag2jet_withFSR.SetLabelSize(0)

    h_mBB_2tag2jet_withFSR.Draw("HIST")
    h_mBB_2tag2jet.Draw("HIST SAME")

    leg = TLegend(0.60, 0.7, 0.90, 0.90)
    leg.SetShadowColor(10)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.AddEntry(h_mBB_2tag2jet,         "No FSR Correction", "lp")
    leg.AddEntry(h_mBB_2tag2jet_withFSR, "With FSR Correction", "lp")
    leg.Draw("SAME")

    pad_2.cd()
    h_mBB_2tag2jet_ratio = h_mBB_2tag2jet_withFSR.Clone("h_mBB_2tag"+ jet +"jet_withFSR_ratio")
    h_mBB_2tag2jet_withFSR_ratio = h_mBB_2tag2jet.Clone("h_mBB_2tag"+ jet +"jet_ratio")
    h_mBB_2tag2jet_ratio.Divide(h_mBB_2tag2jet_withFSR_ratio)
    h_mBB_2tag2jet_ratio.GetYaxis().SetTitle("FSR Corr / Corr")
    h_mBB_2tag2jet_ratio.GetXaxis().SetTitle("mBB [GeV]")
    h_mBB_2tag2jet_ratio.SetTitle("")
    h_mBB_2tag2jet_ratio.SetMinimum(0.5)
    h_mBB_2tag2jet_ratio.SetMaximum(1.5)
    h_mBB_2tag2jet_ratio.SetLabelSize(0.07)
    h_mBB_2tag2jet_ratio.GetXaxis().SetTitleSize(0.07)    
    h_mBB_2tag2jet_ratio.GetXaxis().SetTitleOffset(1.2)
    h_mBB_2tag2jet_ratio.GetYaxis().SetTitleSize(0.07)  
    h_mBB_2tag2jet_ratio.GetYaxis().SetTitleOffset(0.6)
    

    h_mBB_2tag2jet_ratio.Draw("HIST")

    line3=TLine(h_mBB_2tag2jet_ratio.GetBinLowEdge(1), 1, h_mBB_2tag2jet_ratio.GetBinLowEdge(h_mBB_2tag2jet_ratio.GetNbinsX()+1), 1)
    line3.SetLineColor(922)
    line3.SetLineStyle(2)
    line3.SetLineWidth(1)
    line3.Draw("SAME")

    
    c.SaveAs("OneDplots/" + sample + "_mBB_2tag" + jet + "jet_withFSR.png")
    c.SaveAs("OneDplots/" + sample + "_mBB_2tag" + jet + "jet_withFSR.pdf")

