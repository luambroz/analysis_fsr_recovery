SDIR  = ../src/
IDIR  = include/
LDIR  = ../run/

MAKEFLAGS = --no-print-directory -r -s
INCLUDE = $(shell root-config --cflags)
LIBS    = $(shell root-config --libs) -lTMVA -lMLP -lTreePlayer -lMinuit


BINS7 = roo
OBJS = 
all: $(BINS7)

$(BINS7): % : main.C
	@echo -n "Building $@ ... "
	$(CXX) $(CCFLAGS) $< -I$(IDIR) $(INCLUDE) $(LIBS) LucaClass.C TH1Fs/TH1Fs.C TH2Fs/TH2Fs.C -o execute
	@echo "Done"

clean:
	rm -f $(BINS)
