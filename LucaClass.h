#ifndef LucaClass_h
#define LucaClass_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include <TH1F.h>
#include <TH2F.h>
#include "TH1Fs/TH1Fs.h"
#include "TH2Fs/TH2Fs.h"

class LucaClass {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           nTags;
   Int_t           nTaus;
   Int_t           b1Flav;
   Int_t           b2Flav;
   Int_t           j3Flav;
   Int_t           nFatJets;
   Int_t           nTrkJets;
   Int_t           RunNumber;
   Int_t           ChannelNumber;
   Int_t           nbTagsOutsideFJ;
   Double_t        HT;
   Double_t        nJ;
   Double_t        MET;
   Double_t        MPT;
   Double_t        mB1;
   Double_t        mB2;
   Double_t        mBB;
   Double_t        mJ3;
   Double_t        mva;
   Double_t        yBB;
   Double_t        dRBB;
   Double_t        mBBJ;
   Double_t        pTB1;
   Double_t        pTB2;
   Double_t        pTBB;
   Double_t        pTJ3;
   Double_t        etaB1;
   Double_t        etaB2;
   Double_t        etaJ3;
   Double_t        phiB1;
   Double_t        phiB2;
   Double_t        phiJ3;
   Double_t        dEtaBB;
   Double_t        dPhiBB;
   Double_t        ActualMu;
   Double_t        MV2c10B1;
   Double_t        MV2c10B2;
   Double_t        MV2c10J3;
   Double_t        SumPtJet;
   Double_t        AverageMu;
   Double_t        dPhiMETMPT;
   Double_t        mvadiboson;
   Double_t        EventNumber;
   Double_t        EventWeight;
   Double_t        bin_MV2c10B1;
   Double_t        bin_MV2c10B2;
   Double_t        bin_MV2c10J3;
   Double_t        dPhiMETdijet;
   Double_t        MindPhiMETJet;
   Double_t        ActualMuScaled;
   Double_t        AverageMuScaled;
   Double_t        EventNumberMod2;
   Int_t           nbTagsInFJ;
   Int_t           nTrkjetsInFJ;
   Int_t           FSRflag;

   // List of branches
   TBranch        *b_nTags;   //!
   TBranch        *b_nTaus;   //!
   TBranch        *b_b1Flav;   //!
   TBranch        *b_b2Flav;   //!
   TBranch        *b_j3Flav;   //!
   TBranch        *b_nFatJets;   //!
   TBranch        *b_nTrkJets;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_ChannelNumber;   //!
   TBranch        *b_nbTagsOutsideFJ;   //!
   TBranch        *b_HT;   //!
   TBranch        *b_nJ;   //!
   TBranch        *b_MET;   //!
   TBranch        *b_MPT;   //!
   TBranch        *b_mB1;   //!
   TBranch        *b_mB2;   //!
   TBranch        *b_mBB;   //!
   TBranch        *b_mJ3;   //!
   TBranch        *b_mva;   //!
   TBranch        *b_yBB;   //!
   TBranch        *b_dRBB;   //!
   TBranch        *b_mBBJ;   //!
   TBranch        *b_pTB1;   //!
   TBranch        *b_pTB2;   //!
   TBranch        *b_pTBB;   //!
   TBranch        *b_pTJ3;   //!
   TBranch        *b_etaB1;   //!
   TBranch        *b_etaB2;   //!
   TBranch        *b_etaJ3;   //!
   TBranch        *b_phiB1;   //!
   TBranch        *b_phiB2;   //!
   TBranch        *b_phiJ3;   //!
   TBranch        *b_dEtaBB;   //!
   TBranch        *b_dPhiBB;   //!
   TBranch        *b_ActualMu;   //!
   TBranch        *b_MV2c10B1;   //!
   TBranch        *b_MV2c10B2;   //!
   TBranch        *b_MV2c10J3;   //!
   TBranch        *b_SumPtJet;   //!
   TBranch        *b_AverageMu;   //!
   TBranch        *b_dPhiMETMPT;   //!
   TBranch        *b_mvadiboson;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_EventWeight;   //!
   TBranch        *b_bin_MV2c10B1;   //!
   TBranch        *b_bin_MV2c10B2;   //!
   TBranch        *b_bin_MV2c10J3;   //!
   TBranch        *b_dPhiMETdijet;   //!
   TBranch        *b_MindPhiMETJet;   //!
   TBranch        *b_ActualMuScaled;   //!
   TBranch        *b_AverageMuScaled;   //!
   TBranch        *b_EventNumberMod2;   //!
   TBranch        *b_nbTagsInFJ;   //!
   TBranch        *b_nTrkjetsInFJ;   //!
   TBranch        *b_FSRflag; //!

   LucaClass(TString sample);
   virtual ~LucaClass();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   void Write(TString fOutName);
   TString m_sample;

   //Histograms
   TH1F *h_mBB_2tag2jet             = new  TH1F("h_" + m_sample + "_mBB_2tag2jet", "h_mBB_2tag2jet", 100, 0, 500);
   TH1F *h_mBB_2tag2jet_withFSR     = new  TH1F("h_" + m_sample + "_mBB_2tag2jet_withFSR", "h_mBB_2tag2jet_withFSR", 100, 0, 500);
   TH1F *h_mBB_2tag3jet             = new  TH1F("h_" + m_sample + "_mBB_2tag3jet", "h_mBB_2tag3jet", 100, 0, 500);
   TH1F *h_mBB_2tag3jet_withFSR     = new  TH1F("h_" + m_sample + "_mBB_2tag3jet_withFSR", "h_mBB_2tag3jet_withFSR", 100, 0, 500);

   TH1Fs *h_mBBJ                    = new TH1Fs("h_" + m_sample + "_mBBJ", "h_mBBJ", 50, 0, 500, "mBBJ [GeV]");
   TH1Fs *h_pTJ3                    = new TH1Fs("h_" + m_sample + "_pTJ3", "h_pTJ3", 30, 0, 300, "pTJ3 [GeV]"); 
   TH1Fs *h_MV2c10J3                = new TH1Fs("h_" + m_sample + "_MV2c10J3", "h_MV2c10J3", 50, -1, 1);
   TH1Fs *h_j3Flav                  = new TH1Fs("h_" + m_sample + "_j3Flav", "h_j3Flav", 50, -25, 25);
   TH1Fs *h_HT                      = new TH1Fs("h_" + m_sample + "_HT", "h_HT", 50, 200, 700, "HT [GeV]");
   TH1Fs *h_SumPtJet                = new TH1Fs("h_" + m_sample + "_SumPtJet", "h_SumPtJet", 50, 0, 500, "SumPtJet [GeV]");
   TH1Fs *h_MET                     = new TH1Fs("h_" + m_sample + "_MET", "h_MET", 50, 0, 500, "MET [GeV]");
   TH1Fs *h_SumMV2c10               = new TH1Fs("h_" + m_sample + "_SumMV2c10", "h_SumMV2c10", 90, 0, 3);

   TH1Fs *h_dR_bj_min               = new TH1Fs("h_" + m_sample + "_dR_bj_min", "h_dR_bj_min", 40, 0, 5, "min(dRbj)");       
   TH1Fs *h_dR_bj_max               = new TH1Fs("h_" + m_sample + "_dR_bj_max", "h_dR_bj_max", 40, 0, 5, "max(dRbj)");
   TH1Fs *h_dR_j_bb                 = new TH1Fs("h_" + m_sample + "_dR_j_bb", "h_dR_j_bb", 40, 0, 5, "dRbb_j)");

   TH1Fs *h_dR_bj_max_Div_dR_bb     = new TH1Fs("h_" + m_sample + "_dR_bj_max_Div_dR_bb", "h_dR_bj_max_Div_dR_bb", 20, 0, 5);
   TH1Fs *h_dEta_bj_max_Div_dEta_bb = new TH1Fs("h_" + m_sample + "_dEta_bj_max_Div_dEta_bb", "h_dEta_bj_max_Div_dEta_bb", 20, 0, 5); 
   TH1Fs *h_dPhi_bj_max_Div_dPhi_bb = new TH1Fs("h_" + m_sample + "_dPhi_bj_max_Div_dPhi_bb", "h_dPhi_bj_max_Div_dPhi_bb", 20, 0, 5);  

   TH1Fs *h_pt3_Div_ptb_min         = new TH1Fs("h_" + m_sample + "_pt3_Div_ptb_min", "h_pt3_Div_ptb_min", 20, 0, 5);
   TH1Fs *h_pt3_Div_ptb_max         = new TH1Fs("h_" + m_sample + "_pt3_Div_ptb_max", "h_pt3_Div_ptb_max", 20, 0, 5);
   TH1Fs *h_pt3_Div_ptbb            = new TH1Fs("h_" + m_sample + "_pt3_Div_ptbb", "h_pt3_Div_ptbb", 20, 0, 5);

   TH1Fs *h_pt3_Div_ptb_closest_b   = new TH1Fs("h_" + m_sample + "_pt3_Div_ptb_closest_b", "h_pt3_Div_ptb_closest_b", 20, 0, 5);
   TH1Fs *h_pt3_Div_ptb_farthest_b  = new TH1Fs("h_" + m_sample + "_pt3_Div_ptb_farthest_b", "h_pt3_Div_ptb_farthest_b", 20, 0, 5);

   TH1Fs *h_z_star                  = new TH1Fs("h_" + m_sample + "_z_star", "h_z_star", 50, 0, 20);

   TH2Fs *h_dR_bj_min_dR_bj_max     = new TH2Fs("h_" + m_sample + "_dR_bj_min_dR_bj_max", "h_dR_bj_min_dR_bj_max", 20, 0, 2, 20, 0, 2, "min(dRbj)", "max(dRbj)");
   TH2Fs *h_pt3_Div_ptb_closest_b_pt3_Div_ptb_farthest_b = new TH2Fs("h_" + m_sample + "_pt3_Div_ptb_closest_b_pt3_Div_ptb_farthest_b", "h_pt3_Div_ptb_closest_b_pt3_Div_ptb_farthest_b", 10, 0, 1, 10, 0, 1, "ptj3 / pt_closest_b", "ptj3 / pt_farthest_b");
   TH2Fs *h_pt3_Div_ptb_max_pt3_Div_ptb_min = new TH2Fs("h_" + m_sample + "_pt3_Div_ptb_max_pt3_Div_ptb_min", "h_pt3_Div_ptb_max_pt3_Div_ptb_min", 10, 0, 1, 10, 0, 1, "ptj3 / ptb1", "ptj3 / ptb2"); 
   
  TH2Fs *h_Eta_bj_max_Div_dEta_bb_dPhi_bj_max_Div_dPhi_bb = new TH2Fs("h_" + m_sample + "_Eta_bj_max_Div_dEta_bb_dPhi_bj_max_Div_dPhi_bb", "h_Eta_bj_max_Div_dEta_bb_dPhi_bj_max_Div_dPhi_bb", 20, 0, 5, 20, 0, 5, "dEta_bj_max/dEta_bb", "dPhi_bj_max/dPhi_bb");

  TH2Fs *h_abs_etaJ3_etaB1_acos_cos_phiJ3_phiB1 = new TH2Fs("h_" + m_sample + "_abs_etaJ3_etaB1_acos_cos_phiJ3_phiB1", "h_abs_etaJ3_etaB1_acos_cos_phiJ3_phiB1", 20, 0, 1, 20, 0, 1, "abs(etaJ3-etaB1)", "acos(cos(phiJ3-phiB1))");

  TH2Fs *h_mBB_mBBJ                 = new TH2Fs("h_" + m_sample + "_mBB_mBBJ", "h_mBB_mBBJ", 60, 0, 300, 60, 0, 300, "mBB[GeV]", "mBBJ[GeV]");

};

#endif

#ifdef LucaClass_cxx
LucaClass::LucaClass(TString sample) : m_sample(sample)
{

   TFile *f = new TFile(sample + ".root");
   TTree *tree = (TTree *) f->Get("Nominal");
   Init(tree);

}

LucaClass::~LucaClass()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t LucaClass::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t LucaClass::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
   }
   return centry;
}

void LucaClass::Init(TTree *tree)
{

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("nTags", &nTags, &b_nTags);
   fChain->SetBranchAddress("nTaus", &nTaus, &b_nTaus);
   fChain->SetBranchAddress("b1Flav", &b1Flav, &b_b1Flav);
   fChain->SetBranchAddress("b2Flav", &b2Flav, &b_b2Flav);
   fChain->SetBranchAddress("j3Flav", &j3Flav, &b_j3Flav);
   fChain->SetBranchAddress("nFatJets", &nFatJets, &b_nFatJets);
   fChain->SetBranchAddress("nTrkJets", &nTrkJets, &b_nTrkJets);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("ChannelNumber", &ChannelNumber, &b_ChannelNumber);
   fChain->SetBranchAddress("nbTagsOutsideFJ", &nbTagsOutsideFJ, &b_nbTagsOutsideFJ);
   fChain->SetBranchAddress("HT", &HT, &b_HT);
   fChain->SetBranchAddress("nJ", &nJ, &b_nJ);
   fChain->SetBranchAddress("MET", &MET, &b_MET);
   fChain->SetBranchAddress("MPT", &MPT, &b_MPT);
   fChain->SetBranchAddress("mB1", &mB1, &b_mB1);
   fChain->SetBranchAddress("mB2", &mB2, &b_mB2);
   fChain->SetBranchAddress("mBB", &mBB, &b_mBB);
   fChain->SetBranchAddress("mJ3", &mJ3, &b_mJ3);
   fChain->SetBranchAddress("mva", &mva, &b_mva);
   fChain->SetBranchAddress("yBB", &yBB, &b_yBB);
   fChain->SetBranchAddress("dRBB", &dRBB, &b_dRBB);
   fChain->SetBranchAddress("mBBJ", &mBBJ, &b_mBBJ);
   fChain->SetBranchAddress("pTB1", &pTB1, &b_pTB1);
   fChain->SetBranchAddress("pTB2", &pTB2, &b_pTB2);
   fChain->SetBranchAddress("pTBB", &pTBB, &b_pTBB);
   fChain->SetBranchAddress("pTJ3", &pTJ3, &b_pTJ3);
   fChain->SetBranchAddress("etaB1", &etaB1, &b_etaB1);
   fChain->SetBranchAddress("etaB2", &etaB2, &b_etaB2);
   fChain->SetBranchAddress("etaJ3", &etaJ3, &b_etaJ3);
   fChain->SetBranchAddress("phiB1", &phiB1, &b_phiB1);
   fChain->SetBranchAddress("phiB2", &phiB2, &b_phiB2);
   fChain->SetBranchAddress("phiJ3", &phiJ3, &b_phiJ3);
   fChain->SetBranchAddress("dEtaBB", &dEtaBB, &b_dEtaBB);
   fChain->SetBranchAddress("dPhiBB", &dPhiBB, &b_dPhiBB);
   fChain->SetBranchAddress("ActualMu", &ActualMu, &b_ActualMu);
   fChain->SetBranchAddress("MV2c10B1", &MV2c10B1, &b_MV2c10B1);
   fChain->SetBranchAddress("MV2c10B2", &MV2c10B2, &b_MV2c10B2);
   fChain->SetBranchAddress("MV2c10J3", &MV2c10J3, &b_MV2c10J3);
   fChain->SetBranchAddress("SumPtJet", &SumPtJet, &b_SumPtJet);
   fChain->SetBranchAddress("AverageMu", &AverageMu, &b_AverageMu);
   fChain->SetBranchAddress("dPhiMETMPT", &dPhiMETMPT, &b_dPhiMETMPT);
   fChain->SetBranchAddress("mvadiboson", &mvadiboson, &b_mvadiboson);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("EventWeight", &EventWeight, &b_EventWeight);
   fChain->SetBranchAddress("bin_MV2c10B1", &bin_MV2c10B1, &b_bin_MV2c10B1);
   fChain->SetBranchAddress("bin_MV2c10B2", &bin_MV2c10B2, &b_bin_MV2c10B2);
   fChain->SetBranchAddress("bin_MV2c10J3", &bin_MV2c10J3, &b_bin_MV2c10J3);
   fChain->SetBranchAddress("dPhiMETdijet", &dPhiMETdijet, &b_dPhiMETdijet);
   fChain->SetBranchAddress("MindPhiMETJet", &MindPhiMETJet, &b_MindPhiMETJet);
   fChain->SetBranchAddress("ActualMuScaled", &ActualMuScaled, &b_ActualMuScaled);
   fChain->SetBranchAddress("AverageMuScaled", &AverageMuScaled, &b_AverageMuScaled);
   fChain->SetBranchAddress("EventNumberMod2", &EventNumberMod2, &b_EventNumberMod2);
   fChain->SetBranchAddress("nbTagsInFJ", &nbTagsInFJ, &b_nbTagsInFJ);
   fChain->SetBranchAddress("nTrkjetsInFJ", &nTrkjetsInFJ, &b_nTrkjetsInFJ);
   fChain->SetBranchAddress("FSRflag", &FSRflag, &b_FSRflag);
}


#endif // #ifdef LucaClass_cxx
